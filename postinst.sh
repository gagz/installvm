#!/bin/sh 

# exit on error
set -e

tar -x -v -C/tmp -f /tmp/postinst.tar

mkdir -m700 -p /root/.ssh

cp /tmp/postinst/authorized_keys /root/.ssh || \
	echo Failed to copy authorized_keys
