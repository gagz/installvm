Setup VMs automatically.

Usage
-----

Get the debian iso to install, and ajust if needed the `ISOPATH` in `create`. Or put all the assets in a path and configure the `ROOT_PATH` variable.
The `ISOPATH` can also be set to a URL.

Copy the ssh public key that you will use in the postinst folder:
```
$ cp ~/.ssh/id_rsa.pub postinst/authorized_keys
```

Install the VM providing the hostname and the VM number (the last block of the IP):
```
$ ./create maquineta 42
```

Profit!!! Everything should work automatically now. You can watch the installer running by connecting to the console:
```
$ virsh console maquineta
```

Params
------

The memory can be set with `-r` (in MB) and the disk space with `-d` (in GB).
You can disable preseed with `-n`.

Credits
-------

This repo is a fork from:
https://git.sindominio.net/meskio/installvm
